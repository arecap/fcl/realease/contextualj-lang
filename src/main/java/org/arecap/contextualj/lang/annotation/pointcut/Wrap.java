/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.contextualj.lang.annotation.pointcut;


/**
 * Marker annotation.
 *
 * Method supported only
 *
 * @{@link org.contextualj.lang.annotation.pointcut.Wrap} annotated methods are
 * subject for a expression expression that matches join points Wrap Advice.
 * Wrap Advice are methods that could be defined in Context Oriented Beans.
 *
 * The Wrap Advice should be triggered containing the join point invocation tree
 * of the Context Bean method to be called.
 *
 * This is most powerful type of advice. The Wrap Advice could break the normal
 * Context Bean method execution process by not calling the joint point invocation tree
 * or if {@link java.lang.Throwable} occurs.
 * The Wrap Advice argument should be defined as a Join Point of the Context Bean subject method
 * that contains all the store about the execution. To wrap Context Bean method parameter for mock ups
 * or others value changes of the normal process execution should be enabled
 *
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 *
 */

import org.arecap.contextualj.lang.annotation.ContextPointcut;

import java.lang.annotation.*;

@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@ContextPointcut
@Documented
public @interface Wrap {
}
