/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.contextualj.lang.annotation.expression;

import java.lang.annotation.*;

/**
 * Marker annotation.
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 */
@Target({ElementType.ANNOTATION_TYPE, ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(LexicalPhrase.class)
@Expression
@Documented
public @interface LexicalExpression {

    /**
     *
     * OR lexical operator
     *
     */
    String OR = "or";

    /**
     *
     * AND lexical operator
     *
     */
    String AND = "and";

    /**
     *
     * IDENTITY lexical operator
     *
     */
    String IDENTITY = "equals";

    /**
     *
     * GT lexical operator
     *
     */
    String GT = ">";

    /**
     *
     * GTE lexical operator
     *
     */
    String GTE = ">=";

    /**
     *
     * LT lexical operator
     *
     */
    String LT = "<";

    /**
     *
     * LTE lexical operator
     *
     */
    String LTE = "<=";

    /**
     *
     * BETWEEN lexical operator
     *
     */
    String BETWEEN = "between";

    /**
     *
     * expression value
     *
     * @return the value expression of a the lexical phrase
     *
     */
    String value();

    /**
     *
     * expression value validation
     *
     * @return the valid expression of the lexical phrase
     *
     *  ex: of lexical phrase
     *
     *  if ( [ {valueExpression} :operator: {validExpression} ] ) is true
     *
     */
    String valid();

    /**
     *
     * lexical operator
     *
     * @return default operator IDENTITY
     *
     */
    String lex() default IDENTITY;

    /**
     *
     * lexical expression composite
     *
     * @return default operator AND
     *
     */
    String composeLex() default AND;

}
